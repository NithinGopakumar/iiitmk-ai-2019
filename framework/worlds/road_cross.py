import random
from collections import defaultdict
from pprint import pprint


class Environment:
    def __init__(self, n_crossings=100, n_lanes=3, speeds=None):
        self.speeds = speeds if speeds is not None else [1, 2, 3]
        # how much can a person see each side. imagine fog on a cold winter morning in Delhi
        self.visibility = n_lanes * max(self.speeds) + 1
        self.no_of_lanes = n_lanes
        # -------------- calculated stuff
        road_len = n_crossings + (4 * self.visibility)
        # if they can't see beyond, no point simulating it
        self.left = 2 * self.visibility
        self.right = road_len - (2 * self.visibility)
        # initialize road
        self.road = [[None] * road_len for _ in range(self.no_of_lanes)]
        # advance the road to some state
        for _ in range(2 * road_len):
            self.step()
        self.positions = {}  # position map for agents

    def step(self):
        new_road = []
        for speed, lane in enumerate(self.road):
            speed = self.speeds[speed]
            new_lane = [None for _ in lane]
            for pos, veh in enumerate(lane):
                if 0 <= pos - speed < len(lane):
                    new_lane[pos - speed] = veh
            source_idx = -1 if speed > 0 else 0
            new_lane[source_idx] = random.choice([None] * 5 + list("🚲🚗🚌🚕🏍🛵🚚"))
            new_road.append(new_lane)
        self.road = new_road

    def spawn(self, agents):
        self.positions = {}
        for position in range(self.left, self.right):
            agent = random.choice(agents)
            side = random.choice(["top", "bottom"])
            self.positions[position, side] = agent

    def observe(self, index, side):
        road = self.road
        if side == "top":
            road = [list(reversed(lane)) for lane in reversed(self.road)]
        return [
            lane[index - self.visibility : index + self.visibility] for lane in road
        ]

    def make_decisions(self):
        decisions = {}
        for (pos, side), agent in self.positions.items():
            decisions[pos, side] = agent.should_cross(self.observe(pos, side))
        killed = self.did_agents_survive(decisions)
        for (pos, side), decision in decisions.items():
            agent = self.positions[pos, side]
            agent.survived += 1 if decision and not killed[pos, side] else 0
            agent.lifetimes += 1

    def did_agents_survive(self, decisions):
        self.step()
        killed = defaultdict(bool)
        for index in range(self.no_of_lanes):
            for (pos, side), decision in decisions.items():
                if decision:
                    if side == "bottom":
                        lane = self.road[index]
                    if side == "top":
                        lane = self.road[self.no_of_lanes - 1 - index]
                    if lane[pos + self.speeds[index]] is not None:
                        killed[pos, side] = True
            self.step()
        return killed


class Agent:
    def __init__(self, name, function):
        self.name = name
        self.should_cross = function
        self.survived = 0
        self.lifetimes = 0

    def __repr__(self):
        return f"{self.name:<10}:{self.survived} / {self.lifetimes}"


# needs prefix "world"
def world_road_cross(**agents):
    agents = [Agent(name, function) for name, function in agents.items()]
    env = Environment()

    n_trials = 100
    for _ in range(n_trials):
        env.spawn(agents)
        env.make_decisions()
        env.step()
    for agent in agents:
        print(agent)


def test():
    def get_decision_dummy(road):
        return True

    def get_decision(road):
        for speed, lane in enumerate(road, start=1):
            if lane[speed] is not None:
                return False
        return True

    agents = {"pvij": get_decision, "pvij_dummy": get_decision_dummy}
    world_road_cross(**agents)


test()
